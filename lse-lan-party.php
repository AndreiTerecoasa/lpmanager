<?php
/**
Plugin Name: LSE Lan Party
Plugin URI: http://andreiterecoasa.ro
Description: Awesome plugin_ \;D/\:D/ \m/
Author: Andrei Terecoasa
Version: 1
Author URI: http://andreiterecoasa.ro
*/

function lp_plugin_setup() {
    $def_lp_options = array(
        'current_ver' => null,
        'current_ver_title' => null,
        'current_ver_date' => null,
        'current_ver_cover' => null,
        'current_ver_rulesnterms' =>null,
        'lp_is_open' => 0,
        'players_table' => null,
        'history_table' => 'lp_lan_party_editions',
        'partners_widget' => null,
        'cosplay_table' => null,
        'cosplay_rulesnterms' => null,
        'games' => array(
                'name' => null,
                'title' => null,
                'is_team' => 0,
                'max_players' => 0,
                'status' => 0,
                'rules_n_terms' => null,
            )
        );

    add_option('lp_options', $def_lp_options);

    $checkforexistingtable = "SHOW TABLES LIKE {$def_lp_options['history_table']}";

    global $wpdb;

    $result = $wpdb->get_var($checkforexistingtable);

    if($result != $def_lp_options['history_table']) {

        $charset_collate = '';

        if ( ! empty( $wpdb->charset ) ) {
          $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }

        if ( ! empty( $wpdb->collate ) ) {
          $charset_collate .= " COLLATE {$wpdb->collate}";
        }

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $create_history_table = 'CREATE TABLE '.$def_lp_options['history_table'].'(
            id int(2) NOT NULL AUTO_INCREMENT,
            nume varchar(200),
            tabel_jucatori varchar(200),
            jocuri_editie varchar(1000),
            jucatori_inscrisi_site int(5),
            echipe_inscrise_site int(5),
            data_editie varchar(12),
            afis_editie varchar(500),
            PRIMARY KEY  id (id)
            )'.$charset_collate.';';

        dbDelta($create_history_table);
    }







    add_role( 'lan_admin', 'LANParty Admin', array( 'read' => true, 'level_0' => true ) );

}


function lp_plugin_deactivation() {
    delete_option('lp_options');
}


function lp_add_to_menu() {
	add_menu_page( 'Lan Party by LSE' , 'Lan Party','read', 'lan-party','lp_main_page','',81);
    add_submenu_page( 'lan-party', 'Jucatori', 'Jucatori', 'read', 'lan-party-players', 'lp_players_page' );
    add_submenu_page( 'lan-party', 'Setari', 'Setari', 'read', 'lan-party-settings', 'lp_settings_page' );
    add_submenu_page( 'lan-party', 'Formulare', 'Formulare', 'read', 'lan-party-forms', 'lp_forms_page' );
    add_submenu_page( 'lan-party', 'Sponsori', 'Sponsori', 'read', 'lan-party-partners', 'lp_partners_page' );
    add_submenu_page( 'lan-party', 'Export Date', 'Export Date', 'read', 'lan-party-export', 'lp_export_page' );
    add_submenu_page( 'lan-party', 'Istoric', 'Istoric', 'read', 'lan-party-history', 'lp_history_page' );
    add_submenu_page( 'lan-party', 'Despre', 'Despre', 'read', 'lan-party-help', 'lp_help_page' );
}

function lp_main_page() {
    include_once 'lan-party-index.php';
}

function lp_settings_page() {
    include_once 'lan-party-settings.php';
}

function lp_help_page() {
    include_once 'lan-party-help.php';
}

function lp_players_page() {
    include_once 'lan-party-players.php';
}

function lp_forms_page() {
    include_once 'lan-party-forms.php';
}

function lp_history_page() {
    include_once 'lan-party-history.php';
}

function lp_partners_page() {
    include_once 'lan-party-partners.php';
}

function lp_export_page() {
    include_once 'lan-party-export.php';
}


function lp_header() {
    $lp_opt = get_option('lp_options');
    ?>
    <section class="header">
        <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>lp_style.css">
        <h1 id="lph_title">LSE Lan Party</h1>
        <h4 id="lph_subtitle"><strong>©</strong> coded with love by Andrei Terecoasa :3</h4>
    <?php if($lp_opt['lp_is_open']) { ?>
            <h3>Editia curenta Lan Party: <span><?php echo $lp_opt['current_ver_title']; ?></span></h3>
            <p>Data: <?php echo $lp_opt['current_ver_date']; ?></p>
    <?php } else { }?>
    </section>
    <?php
}

function lp_add_new_edition() {
    if(isset($_POST['adauga_editie_noua'])) {
        $lp_set = get_option('lp_options');
        $name = $_POST['nume_editie'];
        $titlu = $_POST['titlu_editie'];
        $jocuri = $_POST['jocuri'];
        $type_jocuri = $_POST['type_jocuri'];
        $max_players = $_POST['max_players'];
        $regulament_joc = $_POST['rules_n_terms'];
        $titlu_joc = $_POST['g_title'];
        $data_editie = $_POST['data_editie'];
        $cover_editie = $_POST['cover_editie'];
        $regulament_editie = $_POST['regulament_editie'];
        $regulament_cosplay = $_POST['regulament_cosplay'];


        $l = count($jocuri);


        for($i=0;$i<$l;$i++) {
            $games[$i] = array(
                'name' => $jocuri[$i],
                'title' => $titlu_joc[$i],
                'is_team' => $type_jocuri[$i],
                'max_players' => $max_players[$i],
                'status' => 0,
                'rules_n_terms' => $regulament_joc[$i]
                );
        }


        $table_name = preg_replace('/\s+/', '', $name);
        $table_name = preg_replace('~[.]+~', '', $table_name);
        $table_name = strtolower($table_name);

        $cosplay_table = 'lp_cosplay_'. esc_sql($table_name);

        $table_name1 = 'lp_'. esc_sql($table_name);




        $time = strtotime($data_editie);
        $data_editie =date('d-m-Y',$time);

        global $wpdb;

        $charset_collate = '';

        if ( ! empty( $wpdb->charset ) ) {
          $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }

        if ( ! empty( $wpdb->collate ) ) {
          $charset_collate .= " COLLATE {$wpdb->collate}";
        }

        $create_players_table = 'CREATE TABLE IF NOT EXISTS '.$table_name1.'(
            id  int(10) NOT NULL AUTO_INCREMENT,
            nume  varchar(255),
            prenume  varchar(255),
            email  varchar(255),
            cnp varchar(15),
            telefon  varchar(255),
            domiciliu varchar(500),
            joc  varchar(100),
            nickname varchar(200),
            cu_echipa  int(1),
            echipa  varchar(255),
            capitan int(1),
            ce_aduce  varchar(255),
            date timestamp DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY  id (id)
            )'.$charset_collate.';';




        $create_cosplay_table = 'CREATE TABLE IF NOT EXISTS '.$cosplay_table.'(
            id int(10) NOT NULL AUTO_INCREMENT,
            nume varchar(255),
            prenume varchar(255),
            data_nasterii varchar(255),
            email varchar(255),
            telefon varchar(15),
            personaj varchar(255),
            joc varchar(255),
            PRIMARY KEY  id (id))'.$charset_collate.';';

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $result1 = $wpdb->get_var("SHOW TABLES LIKE '{$table_name}'");
        $result2 = $wpdb->get_var("SHOW TABLES LIKE '{$cosplay_table}'");
        if( ($result1 != $table_name1)&&($result2 != $cosplay_table)) {
            dbDelta( $create_players_table);
            dbDelta( $create_cosplay_table);
            $lp_set['current_ver'] = $table_name1;
            $lp_set['current_ver_title'] = $titlu;
            $lp_set['lp_is_open'] = 1;
            $lp_set['games'] = $games;
            $lp_set['players_table'] = $table_name1;
            $lp_set['current_ver_date'] = $data_editie;
            $lp_set['current_ver_cover'] = $cover_editie;
            $lp_set['current_ver_rulesnterms'] = $regulament_editie;
            $lp_set['cosplay_table'] = $cosplay_table;
            $lp_set['cosplay_rulesnterms'] = $regulament_cosplay;
            update_option('lp_options',$lp_set);
        } else {
            echo '<p>Un tabel cu acest nume exista deja. Foloseste alt nume pentru "Nume editie"!</p>';
        }




    }
}

function lp_update_edition() {
    $lp_opt =   get_option('lp_options');

    if(isset($_POST['update_lp_settings'])) {
        $jocuri = $_POST['jocuri'];
        $titlu_joc = $_POST['g_title'];
        $type_jocuri = $_POST['type_jocuri'];
        $max_players = $_POST['max_players'];
        $status = $_POST['status'];
        $regulament_joc = $_POST['rules_n_terms'];
        $l = count($jocuri);


        for($i=0;$i<$l;$i++) {
            $games[$i] = array(
                'name' => $jocuri[$i],
                'title' => $titlu_joc[$i],
                'is_team' => $type_jocuri[$i],
                'max_players' => $max_players[$i],
                'status' => $status[$i],
                'rules_n_terms' => $regulament_joc
                );
        }

        $temp = $lp_opt;

        $temp['games'] = $games;
        update_option('lp_options',$temp);


    }
}

function lp_cosplay_form() {
    $lp_opt = get_option('lp_options');


    if(isset($_POST['nr_cos'])) {
        $nr = $_POST['nr_cos'];

        $content = '<form action="" method="POST" id="lp_single_form" class="lp_team_form">';
        for($i=0; $i< $nr; $i++) {
            $content .= '<table class="lp_single_form"><tr><td>Nume</td><td><input type="text" name="nume_cosplay[]"></td></tr><tr><td>Prenume</td><td><input type="text" name="prenume_cosplay[]"></td></tr><tr><td>Data nasterii</td><td><input type="text" name="dn_cosplay[]"></td></tr><tr><td>Email</td><td><input type="text" name="email_cosplay[]"></td></tr><tr><td>Telefon</td><td><input type="text" name="telefon_cosplay[]"></td></tr><tr><td>Personaj</td><td><input type="text" name="personaj_cosplay[]"></td></tr><tr><td>Jocul (din care e personajul) </td><td><input type="text" name="joc_cosplay[]"></td></tr><br/></table>';
        }

        $content .= '<table>
                <tr><td colspan="2"><input type="checkbox" name="de_acord_cu_termenii"> Sunt de acord cu <a href="'.$lp_opt['cosplay_rulesnterms'].'">Termenii si Conditiile avand in vedere participarea la Competitia de COSPLAY</a>.</td></tr>
                <tr>
                    <td colspan="2"><input type="submit" name="inscrie_cosplay" value="Inscriere"></td>
                </tr>
            </table>
        </form>
         <script>
            jQuery(document).ready(function() {
                jQuery("#lp_single_form").bootstrapValidator({
                    container: "tooltip",
                    group: "td",
                    feedbackIcons: {
                        valid: "fa fa-check",
                        invalid: "fa fa-close",
                        validating: "fa fa-spinner fa-spin"
                    },
                    fields : {
                        "nume_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "prenume_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "dn_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "email_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "telefon_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "personaj_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "joc_cosplay[]" : {
                            validators : {
                                notEmpty : {
                                    message: "Toate campurile sunt obligatorii"
                                }
                            }
                        },
                        "de_acord_cu_termenii": {
                            validators: {
                                choice: {
                                    min:1,
                                    message: "Trebuie sa fii de acord cu termenii si conditiile"
                                }
                            }
                        }
                    }
                });
            });

        </script>';
        return $content;

    } else if(isset($_POST['inscrie_cosplay'])) {
        $nume = $_POST['nume_cosplay'];
        $prenume = $_POST['prenume_cosplay'];
        $dn = $_POST['dn_cosplay'];
        $email = $_POST['email_cosplay'];
        $telefon = $_POST['telefon_cosplay'];
        $personaj = $_POST['personaj_cosplay'];
        $joc = $_POST['joc_cosplay'];
        $check = 0;
        $l = count($nume);
        for($i=0; $i<$l; $i++) {
            $data = array(
                'nume' => $nume[$i],
                'prenume' => $prenume[$i],
                'data_nasterii' => $dn[$i],
                'email' => $email[$i],
                'telefon' => $telefon[$i],
                'personaj' => $personaj[$i],
                'joc' => $joc[$i]
                );

            global $wpdb;

            $res = $wpdb->insert($lp_opt['cosplay_table'],$data,array('%s','%s','%s','%s','%s','%s','%s'));
            if($res) {$check = $check+1;}

        }

        if($check == $l) {

            $mail_content = '
                        <div id="lp_header">
                            <img src="'.$lp_opt['current_ver_cover'].'" alt="">
                            <h2>'.$lp_opt['current_ver_title'].'</h2>
                        </div>
                        <div class="lp_content">
                             <p>Salut <strong>'.$prenume[0] .' '.$nume[0].'</strong>,</p>
                            <p>Te informam ca inscrierea ta in cadrul '.$lp_opt['current_ver_title'].' pentru competitia de COSPLAY s-a facut cu succes.</p>
                            <p>Iti reamintim ca, prin inscriere, te-ai angajat sa respecti, pe langa regulamentul jocului, si niste reguli generale pe care le gasesti <a href="'.$lp_opt['cosplay_rulesnterms'].'">aici</a>.</p>
                            <br/>
                            <p>Multumim!</p>
                       </div>

            ';

                    $headers[] = 'From: LANParty by LSE <lanparty@lse.org.ro>';
                    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
                    function set_html_content_type() {
                        return 'text/html';
                    }
                    $to = $email[0];
                    $message = stripslashes($mail_content);
                    $subject = 'Inscriere LANParty [by LSE]';
                    if(wp_mail( $to, $subject, $message, $headers )) {
                     return 'Vei primi un mail de confirmare a inscrierii<br/>Te rugam sa verifici daca datele sunt corecte. In caz contrar contacteaza-ne la adresa de mail inscrieri@lanparty.lse.org.ro';
                    }
        } else {
            echo 'eroare';
        }
    } else {
        $content = '<form action="" method="POST">
            <p>Numar cosplayeri: <input type="text" name="nr_cos"></p>
            <p><input type="submit" name="next" value="Next"></p>
        </form>';
        return $content;
    }
}


function signs_up($is_team, $game) {
    $lp_opt =   get_option('lp_options');

    if ($is_team) {
        $sql = 'SELECT DISTINCT `echipa` FROM `'.$lp_opt['players_table'].'` WHERE `cu_echipa`=1 AND `joc`=\''.$game.'\'';
    } else {
        $sql = 'SELECT DISTINCT `id` FROM `'.$lp_opt['players_table'].'` WHERE `cu_echipa`=0 AND `joc`=\''.$game.'\'';
    }

    global $wpdb;

    $result = $wpdb->query($sql);

    return $wpdb->num_rows;
}

function lp_form( $atts ){


    extract(shortcode_atts(
        array(
            'game' => '',
            'is_team' => 0,
            'players_in_team' => 1
            ), $atts));

    $opt = array (
        'game' => $game,
        'is_team' => $is_team,
        'players_in_team' => $players_in_team
        );

    $lp_opt =   get_option('lp_options');

    //formular inscriere jocuri tip single


     $validation1 = '
        <script>
        jQuery(document).ready(function() {
            jQuery("#lp_single_form").bootstrapValidator({
                container: "tooltip",
                group : "td",
                feedbackIcons: {
                    valid: "fa fa-check",
                    invalid: "fa fa-close",
                    validating: "fa fa-spinner fa-spin"
                },
                fields : {
                    "jucator_nume" : {
                        validators: {
                            notEmpty : {
                                message: "Numele este obligatoriu"
                            }
                        }
                    },
                    "jucator_prenume" : {
                        validators: {
                            notEmpty : {
                                message: "Prenumele este obligatoriu"
                            }
                        }
                    },
                    "jucator_cnp" : {
                        validators: {
                            notEmpty : {
                                message : "CNP-ul este obligatoriu"
                            },
                            digits : {
                                message: "CNP-ul este format numai din cifre"
                            },
                            stringLength : {
                                max:13,
                                min:13,
                                message: "CNP-ul este format din 13 cifre"
                            }
                        }
                    },
                    "jucator_nickname" : {
                        validators: {
                            notEmpty : {
                                message: "Nicknameul este obligatoriu"
                            }
                        }
                    },
                    "jucator_email" : {
                        validators : {
                            notEmpty : {
                                message: "Emailul este obligatoriu"
                            },
                            emailAdress: {
                                message: "Formatul introdus nu corespunde unei adrese de mail"
                            }
                        }
                    },
                    "jucator_telefon": {
                        validators : {
                            notEmpty: {
                                message: "Telefonul este obligatoriu"
                            },
                            digits : {
                                message: "Formatul introdus nu corespunde unui numar de telefon"
                            },
                            stringLength : {
                                max:10,
                                min:10,
                                message: "CNP-ul este format din 10 cifre"
                            }
                        }
                    },
                    "jucator_ce_aduce": {
                        validators : {
                            notEmpty: {
                                message: "Este obligatoriu sa aduc o statie pe care sa te poti juca avand in vedere ca aceste LANParty este de tip BYOC"
                            }
                        }

                    },
                    "de_acord_cu_termenii": {
                        validators: {
                            choice: {
                                min:1,
                                message: "Trebuie sa fii de acord cu termenii si conditiile"
                            }
                        }
                    }
                }
            });
        });
        </script>
        ';


    $single_form_markup ='
        <form action="" method="POST" id="lp_single_form">
            <table class="lp_single_form">
                <tr style="display:none">
                    <td>Joc:</td>
                    <td><input type="text" name="jucator_joc" value="'.$opt['game'].'">
                    <input type="hidden" name="jucator_jocdsf" value="'.$opt['game'].'"></td>
                </tr>
                <tr>
                    <td>Nume:</td>
                    <td><input type="text" name="jucator_nume"></td>
                </tr>
                <tr>
                    <td>Prenume:</td>
                    <td><input type="text" name="jucator_prenume"></td>
                </tr>
                <tr>
                    <td>CNP:</td>
                    <td><input type="text" name="jucator_cnp"></td>
                </tr>
                <tr>
                    <td>Nickname</td>
                    <td><input type="text" name="jucator_nickname">
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="jucator_email"></td>
                </tr>
                <tr>
                    <td>Telefon</td>
                    <td><input type="text" name="jucator_telefon"></td>
                </tr>
                <tr>
                    <td>Domiciliu</td>
                    <td><input type="text" name="jucator_domiciliu">
                </tr>
                <tr>
                    <td>Ce aduci:</td>
                    <td><select name="jucator_ce_aduce"><option value="" disabled selected>Ce aduci?</option><option value="laptop">Laptop</option><option value="pc">Calculator</option></select></td>
                </tr>
                 <tr><td colspan="2"><input type="checkbox" name="de_acord_cu_termenii"> Sunt de acord cu <a href="http://lanparty.lse.org.ro/termeni/">Termenii si Conditiile avand in vedere participarea la Competitia de '.title_by_name($opt['game']).'</a>.</td></tr>
                <tr><td colspan="2"><input type="submit" name="jucator_inscrie" value="Inscrie-te" class="button"></td></tr>
            </table>
        </form>
    ';

    $single_form_markup .=$validation1;
    switch($opt['game'])  {
        case "lol" : $nickname = "Nume Summoner";
        break;
        default : $nickname = "Nickname";
        break;
    }

    $team_form_markup ='
        <form action="" method="POST" id="lp_team_form">
            <table class="lp_team_form">
                <tr style="display:none">
                    <td>Joc:</td>
                    <td><input type="text" value="'.$opt['game'].'">
                      <input type="hidden" name="jucator_joc" value="'.$opt['game'].'">
                    </td>
                </tr>
                <tr>
                    <td>Nume Echipa</td>
                    <td><input type="text" name="jucator_echipa"></td>
                </tr>
                <tr><td colspan="2"><br/></td></tr>
                <tr class="head"><td colspan="2">Capitan</td></tr>
                <tr>
                    <td>Nume:</td>
                    <td><input type="text" name="jucator_nume[]"></td>
                </tr>
                <tr>
                    <td>Prenume:</td>
                    <td><input type="text" name="jucator_prenume[]"></td>
                </tr>
                <tr style="display:none">
                    <td>E capitan</td>
                    <td><input type="hidden" name="jucator_capitan[]" value="1"></td>
                </tr>
                <tr>
                    <td>CNP:</td>
                    <td><input type="text" name="jucator_cnp[]"></td>
                </tr>
                <tr>
                    <td>'.$nickname.'</td>
                    <td><input type="text" name="jucator_nickname[]">
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="jucator_email[]"></td>
                </tr>
                <tr>
                    <td>Telefon</td>
                    <td><input type="text" name="jucator_telefon[]"></td>
                </tr>
                <tr>
                    <td>Domiciliu</td>
                    <td><input type="text" name="jucator_domiciliu[]">
                </tr>
                <tr>
                    <td>Ce aduci:</td>
                    <td><select name="jucator_ce_aduce[]"><option value="" disabled selected>Ce aduci?</option><option value="laptop">Laptop</option><option value="pc">Calculator</option></select></td>
                </tr>
                        ';

                for($i=2; $i<=$opt['players_in_team']; $i++) {
                    $team_form_markup .= '
                <tr><td colspan="2"><br/></td></tr>
                <tr class="head"><td colspan="2">Jucator '.$i.'</td></tr>
                <tr>
                    <td>Nume:</td>
                    <td><input type="text" name="jucator_nume[]"></td>
                </tr>
                <tr>
                    <td>Prenume:</td>
                    <td><input type="text" name="jucator_prenume[]"></td>
                </tr>
                <tr style="display:none">
                    <td>E capitan</td>
                    <td><input type="hidden" name="jucator_capitan[]" value="0"></td>
                </tr>
                <tr>
                    <td>CNP:</td>
                    <td><input type="text" name="jucator_cnp[]"></td>
                </tr>
                <tr>
                    <td>'.$nickname.'</td>
                    <td><input type="text" name="jucator_nickname[]">
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="jucator_email[]"></td>
                </tr>
                <tr>
                    <td>Telefon</td>
                    <td><input type="text" name="jucator_telefon[]"></td>
                </tr>
                <tr>
                    <td>Domiciliu</td>
                    <td><input type="text" name="jucator_domiciliu[]">
                </tr>
                <tr>
                    <td>Ce aduci:</td>
                    <td><select name="jucator_ce_aduce[]"><option disabled selected>Ce aduci?</option><option value="laptop">Laptop</option><option value="pc">Calculator</option></select></td>
                </tr>

                    ';
                }



        $team_form_markup .='
                <tr><td colspan="2"><input type="checkbox" name="de_acord_cu_termenii"> Sunt de acord cu <a href="http://lanparty.lse.org.ro/termeni/">Termenii si Conditiile avand in vedere participarea la Competitia de '.title_by_name($opt['game']).'</a></td></tr>
                <tr><td colspan="2"><input type="submit" name="echipa_inscrie" value="Inscrie Echipa" class="button"></td></tr>
                </table>
            </form>
        ';


        $validation2 = '
        <script>
        jQuery(document).ready(function() {
            jQuery("#lp_team_form").bootstrapValidator({
                container: "tooltip",
                group : "td",
                feedbackIcons: {
                    valid: "fa fa-check",
                    invalid: "fa fa-close",
                    validating: "fa fa-spinner fa-spin"
                },
                fields : {
                    "jucator_nume[]" : {
                        validators: {
                            notEmpty : {
                                message: "Numele este obligatoriu"
                            }
                        }
                    },
                    "jucator_prenume[]" : {
                        validators: {
                            notEmpty : {
                                message: "Prenumele este obligatoriu"
                            }
                        }
                    },
                    "jucator_cnp[]" : {
                        validators: {
                            notEmpty : {
                                message : "CNP-ul este obligatoriu"
                            },
                            digits : {
                                message: "CNP-ul este format numai din cifre"
                            },
                            stringLength : {
                                max:13,
                                min:13,
                                message: "CNP-ul este format din 13 cifre"
                            }
                        }
                    },
                    "jucator_nickname[]" : {
                        validators: {
                            notEmpty : {
                                message: "Nicknameul este obligatoriu"
                            }
                        }
                    },
                    "jucator_email[]" : {
                        validators : {
                            notEmpty : {
                                message: "Emailul este obligatoriu"
                            },
                            emailAdress: {
                                message: "Formatul introdus nu corespunde unei adrese de mail"
                            }
                        }
                    },
                    "jucator_telefon[]": {
                        validators : {
                            notEmpty: {
                                message: "Telefonul este obligatoriu"
                            },
                            digits : {
                                message: "Formatul introdus nu corespunde unui numar de telefon"
                            },
                            stringLength : {
                                max:10,
                                min:10,
                                message: "CNP-ul este format din 10 cifre"
                            }
                        }
                    },
                    "jucator_ce_aduce[]": {
                        validators : {
                            notEmpty: {
                                message: "Este obligatoriu sa aduc o statie pe care sa te poti juca avand in vedere ca aceste LANParty este de tip BYOC"
                            }
                        }

                    },
                    "de_acord_cu_termenii": {
                        validators: {
                            choice: {
                                min:1,
                                message: "Trebuie sa fii de acord cu termenii si conditiile"
                            }
                        }
                    }
                }
            });
        });
        </script>
        ';
        $team_form_markup .=$validation2;


    if(isset($_POST['jucator_inscrie'])) {
        $data = array(
            'nume' => $_POST['jucator_nume'],
            'prenume' => $_POST['jucator_prenume'],
            'cnp' => $_POST['jucator_cnp'],
            'email' => $_POST['jucator_email'],
            'telefon' => $_POST['jucator_telefon'],
            'domiciliu' => $_POST['jucator_domiciliu'],
            'joc' => $_POST['jucator_joc'],
            'nickname' => $_POST['jucator_nickname'],
            'cu_echipa' => 0,
            'echipa' => '',
            'ce_aduce' => $_POST['jucator_ce_aduce']
            );


        if(!lp_reached_max_players($_POST['jucator_joc'])) {
             if(lp_check_mail($_POST['jucator_email'])) {
            $mesaj = "Un jucator cu acest mail este deja inscris. Daca nu esti tu te rugam contacteaza-ne pentru a rezolva aceasta problema";
            } else {
                global $wpdb;
                $insertresult = $wpdb->insert(
                    $lp_opt['players_table'],
                    $data,
                    array(
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%d',
                        '%s',
                        '%s'
                        )
                    );
                if($insertresult) {
                    $mesaj = 'Vei primi un mail de confirmare a inscrierii<br/>';
                    $mesaj .='Te rugam sa verifici daca datele sunt corecte. In caz contrar contacteaza-ne la adresa de mail inscrieri@lanparty.lse.org.ro';

                    $to = $_POST['jucator_email'];
                    $subject = 'Inscriere LANParty [by LSE]';
                    $message = '
                        <style>

                        </style>
                        <div id="lp_header">
                            <img src="'.$lp_opt['current_ver_cover'].'" alt="">
                            <h2>'.$lp_opt['current_ver_title'].'</h2>
                        </div>
                        <div id="lp_content">
                            <p>Salut <strong>'.$_POST['jucator_prenume'] .' '.$_POST['jucator_nume'].'</strong>,</p>
                            <p>Te informam ca inscrierea ta in cadrul '.$lp_opt['current_ver_title'].' pentru jocul '.title_by_name($_POST['jucator_joc']).' s-a facut cu succes.</p>
                            <p>Iti reamintim ca, prin inscriere, te-ai angajat sa respecti, pe langa regulamentul jocului, si niste reguli generale pe care le gasesti <a href="'.$lp_opt['current_ver_rulesnterms'].'">aici</a>.</p>
                            <br/>
                            <p>Verifica-le atent, caci, in ziua evenimentului, acestea trebuie sa coincida cu cele din actul tau de identitate.  Daca vei constata greseli, trimite-ne un mail la adresa inscrieri@lanparty.lse.org.ro.</p>
                            <p>Iti multumim</p>
                        <table>
                            <tr>
                                <td>Joc:</td>
                                <td>'.title_by_name($_POST['jucator_joc']).'
                            </tr>
                            <tr>
                                <td>Nume:</td>
                                <td>'.$_POST['jucator_nume'].'</td>
                            </tr>
                            <tr>
                                <td>Prenume:</td>
                                <td>'.$_POST['jucator_prenume'].'</td>
                            </tr>
                            <tr>
                                <td>CNP:</td>
                                <td>'.$_POST['jucator_cnp'].'</td>
                            </tr>
                            <tr>
                                <td>Nickname:</td>
                                <td>'.$_POST['jucator_nickname'].'</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>'.$_POST['jucator_email'].'</td>
                            </tr>
                            <tr>
                                <td>Telefon</td>
                                <td>'.$_POST['jucator_telefon'].'</td>
                            </tr>
                            <tr>
                                <td>Domiciliu</td>
                                <td>'.$_POST['jucator_domiciliu'].'</td>
                            </tr>
                            <tr>
                                <td>Ce aduci:</td>
                                <td>'.$_POST['jucator_ce_aduce'].'</td>
                            </tr>
                        </table>
                        </div>
                    ';
                    $headers[] = 'From: LANParty by LSE <lanparty@lse.org.ro>';
                    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
                    function set_html_content_type() {
                        return 'text/html';
                    }

                    $message = stripslashes($message);
                    wp_mail( $to, $subject, $message, $headers );
                } else {
                    $mesaj .="Eroare";
                }
            }
        } else {
             $mesaj = "S-a atins numarul maxim de jucatori. Ne pare rau!";
        }

        return '<p>'.$mesaj.'</p>';
    }

    elseif(isset($_POST['echipa_inscrie'])) {
        $array_nume = $_POST['jucator_nume'];
        $array_prenume = $_POST['jucator_prenume'];
        $array_email = $_POST['jucator_email'];
        $array_cnp = $_POST['jucator_cnp'];
        $array_nickname = $_POST['jucator_nickname'];
        $array_telefon = $_POST['jucator_telefon'];
        $array_domiciliu =  $_POST['jucator_domiciliu'];
        $joc = $_POST['jucator_joc'];
        $array_ce_aduce = $_POST['jucator_ce_aduce'];
        $nume_echipa = $_POST['jucator_echipa'];
        $array_capitan = $_POST['jucator_capitan'];


        $l = count($array_nume);
        if(!lp_reached_max_players($joc)) {
            if(!lp_check_team_name($nume_echipa)) {
                $check_all_inserts = 0;
                for($i=0; $i<$l; $i++) {
                    $data = array(
                        'nume' => $array_nume[$i],
                        'prenume' => $array_prenume[$i],
                        'cnp' => $array_cnp[$i],
                        'email' => $array_email[$i],
                        'telefon' => $array_telefon[$i],
                        'domiciliu' => $array_domiciliu[$i],
                        'joc' => $joc,
                        'nickname' => $array_nickname[$i],
                        'cu_echipa' => 1,
                        'capitan' => $array_capitan[$i],
                        'echipa' => $nume_echipa,
                        'ce_aduce' => $array_ce_aduce[$i]
                        );
                   global $wpdb;
                    $insertresult = $wpdb->insert(
                        $lp_opt['players_table'],
                        $data,
                        array(
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%d',
                            '%d',
                            '%s',
                            '%s'
                            )
                        );
                    if($insertresult) {
                        $check_all_inserts = $check_all_inserts + 1;
                    } else {
                        $mesaj .="eroare";
                    }

                }
                if($check_all_inserts == $l) {
                    $mesaj = 'Vei primi un mail de confirmare a inscrierii<br/>';
                    $mesaj .='Te rugam sa verifici daca datele sunt corecte. In caz contrar contacteaza-ne la adresa de mail inscrieri@lanparty.lse.org.ro';

                    $to = $_POST['jucator_email'];
                    $subject = 'Inscriere LANParty [by LSE]';
                    $message = '
                        <style>

                        </style>
                        <div id="lp_header">
                            <img src="'.$lp_opt['current_ver_cover'].'" alt="">
                            <h2>'.$lp_opt['current_ver_title'].'</h2>
                        </div>
                        <div id="lp_content">
                            <p>Salut <strong>'.$array_prenume[0] .' '.$array_nume[0].'</strong>,</p>
                            <p>Te informam ca inscrierea ta in cadrul '.$lp_opt['current_ver_title'].' pentru jocul '.title_by_name($joc).' s-a facut cu succes.</p>
                            <p>Iti reamintim ca, prin inscriere, te-ai angajat sa respecti, pe langa regulamentul jocului, si niste reguli generale pe care le gasesti <a href="'.$lp_opt['current_ver_rulesnterms'].'">aici</a>.</p>
                            <br/>
                            <p>Mai jos se afla copia formularului completat de tine. Verifica-le atent, caci, in ziua evenimentului, acestea trebuie sa coincida cu cele din actul tau de identitate.  Daca vei constata greseli, trimite-ne un mail la adresa inscrieri@lanparty.lse.org.ro.</p>
                            <p>Iti multumim.</p>';

                        for($i=0; $i<$l; $i++) {
                            $message .='
                            <table>
                                <tr>
                                    <td>Joc:</td>
                                    <td>'.$joc.'
                                </tr>
                                <tr>
                                    <td>Nume:</td>
                                    <td>'.$array_nume[$i].'</td>
                                </tr>
                                <tr>
                                    <td>Prenume:</td>
                                    <td>'.$array_prenume[$i].'</td>
                                </tr>
                                <tr>
                                    <td>CNP:</td>
                                    <td>'.$array_cnp[$i].'</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>'.$array_email[$i].'</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>'.$array_nickname[$i].'</td>
                                </tr>
                                <tr>
                                    <td>Telefon</td>
                                    <td>'.$array_telefon[$i].'</td>
                                </tr>
                                <tr>
                                    <td>Domiciliu</td>
                                    <td>'.$array_domiciliu[$i].'</td>
                                </tr>
                                <tr>
                                    <td>Ce aduci:</td>
                                    <td>'.$array_ce_aduce[$i].'</td>
                                </tr>
                                <tr><td colspan="2"><br/></tr>
                            </table>';
                        }
                        $message .='
                        </div>
                        ';
                        $headers[] = 'From: LANParty by LSE <lanparty@lse.org.ro>';
                        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
                        function set_html_content_type() {
                            return 'text/html';
                        }

                        $message = stripslashes($message);

                        wp_mail( $to, $subject, $message, $headers );
                } else {
                    return 'ups';
                }
            } else {
                $mesaj = "O echipa cu acest nume exista deja. Te rugam verifica mailul pentru confirmarea unei registrari anterioare sau foloseste alt nume";
            }
        }   else {
               $mesaj = "S-a atins numarul maxim de echipe. Ne pare rau!";
            }

        return '<p>'.$mesaj.'</p>';
    }

    elseif (status_by_game($opt['game'])) {
        if($opt['is_team']) {
           return $team_form_markup;
        } else {
            return $single_form_markup;
        }
    } else {
        return "Inscrierile pentru acest joc au fost inchise ori nu au fost deschise inca";
    }

}

function lp_check_mail($mail) {
    $lp_opt =   get_option('lp_options');
    global $wpdb;

    $mail_exists = $wpdb->get_var("SELECT COUNT(*) FROM {$lp_opt['players_table']} WHERE `email`='{$mail}' AND `cu_echipa`=0");
    return $mail_exists;
}

function lp_check_team_name($name) {
    $lp_opt =   get_option('lp_options');
    global $wpdb;

    $team_exists = $wpdb->get_var("SELECT count(*) FROM {$lp_opt['players_table']} WHERE `echipa`='{$name}'");
    return $team_exists;
}

function status_by_game($game_name) {
    $lp_opt =   get_option('lp_options');
    if($lp_opt['lp_is_open']) {
        $games = $lp_opt['games'];
        foreach($games as $game) {
            if($game['name']===$game_name) {
                $val =  $game['status'];
                return $val;
                break;
            }
        }
    }
}


function title_by_name($game_name) {
    $lp_opt =   get_option('lp_options');
    if($lp_opt['lp_is_open']) {
        $games = $lp_opt['games'];
        foreach($games as $game) {
            if($game['name']===$game_name) {
                $val =  $game['title'];
                return $val;
                break;
            }
        }
    }
}

function rules_by_name($game_name) {
    $lp_opt =   get_option('lp_options');
    if($lp_opt['lp_is_open']) {
        $games = $lp_opt['games'];
        foreach($games as $game) {
            if($game['name']===$game_name) {
                $val =  $game['rules_n_terms'];
                return $val;
                break;
            }
        }
    }
}

function lp_reached_max_players($game_name) {
    $lp_opt = get_option('lp_options');

    $games = $lp_opt['games'];
    foreach($games as $game) {
        if($game['name']===$game_name) {
            $max =  $game['max_players'];
            global $wpdb;
            if($game['is_team']) {
                $val = $wpdb->get_var("SELECT COUNT(*) FROM {$lp_opt['players_table']} WHERE `cu_echipa`=1");
                if($val >= $max) {
                    return true;
                } else {
                    return false;
                }
            } else {
                $val = $wpdb->get_var("SELECT COUNT(*) FROM {$lp_opt['players_table']} WHERE `cu_echipa`=0");
                if($val >= $max) {
                    return true;
                } else {
                    return false;
                }
            }

        }
    }
}


function partners_widget($atts) {
    $lp_opt = get_option('lp_options');
    $content = '<div class="lp_sp_widget">';

    $content_temp = stripslashes( $lp_opt['partners_widget'] );
    $content .=$content_temp.'</div>';
    return $content;
}

function lp_count($station) {
    $lp_opt = get_option('lp_options');
    if(!$lp_opt['lp_is_open']) {
        $nr = 0;
    } else {
         global $wpdb;
         $nr =  $wpdb->get_var("SELECT COUNT(*) FROM `{$lp_opt['players_table']}` WHERE `ce_aduce`='{$station}'");
    }

    return $nr;
}

function lp_export_html() {
    $lp_opt = get_option('lp_options');
    global $wpdb;
    $inputs = '';
    $data = $wpdb->get_results("SELECT * FROM {$lp_opt['players_table']}", ARRAY_A);
    $flag = false;
    foreach($data as $row) {

        if(!$flag) {
            $inputs = implode(',', array_keys($row));
            $flag = true;
            break;
        } else {

        }

    }

    $checkboxs = '';
    $inputs_arr =explode(',',$inputs);
    foreach($inputs_arr as $one) {
        $checkboxs .= '<tr>';
        $checkboxs .='<td>'.$one.'</td><td><input type="checkbox" name="fields[]" value="`'.$one.'`"></td>';
        $checkboxs .='</tr>';
    }

    return $checkboxs;

}

add_action('init', 'clean_output_buffer');
function clean_output_buffer() {
        ob_start();
}

function cleanData($str) {
      // Original PHP code by Chirp Internet: www.chirp.com.au
      // Please acknowledge use of this code by including this header.

        // escape tab characters
        $str = preg_replace("/\t/", "\\t", $str);

        // escape new lines
        $str = preg_replace("/\r?\n/", "\\n", $str);

        // convert 't' and 'f' to boolean values
        if($str == 't') $str = 'TRUE';
        if($str == 'f') $str = 'FALSE';

        // force certain number/date formats to be imported as strings
        if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
          $str = "'$str";
        }

        // escape fields that include double quotes
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';

}

function lp_players_pagination($per_pg=10,$pg=1,$url='?'){
    $lp_opt =   get_option('lp_options');

    global $wpdb;
    $query = "SELECT COUNT(*) FROM {$lp_opt['players_table']}";
    $total = $wpdb->get_var($query);
    $adjacents = "2";

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";
    $lastlabel = "Last &rsaquo;&rsaquo;";

    $pg = ($pg == 0 ? 1 : $pg);
    $start = ($pg - 1) * $per_pg;

    $prev = $pg - 1;
    $next = $pg + 1;

    $lastpg = ceil($total/$per_pg);

    $lpm1 = $lastpg - 1; // //last pg minus 1

    $pagination = "";
    if($lastpg > 1){
        $pagination .= "<ul class='pagination'>";
        $pagination .= "<li class='page_info'>Page {$pg} of {$lastpg}</li>";

            if ($pg > 1) $pagination.= "<li><a href='".add_query_arg(array('pg' => $prev))."'>{$prevlabel}</a></li>";

        if ($lastpg < 7 + ($adjacents * 2)){
            for ($counter = 1; $counter <= $lastpg; $counter++){
                if ($counter == $pg)
                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
                else
                    $pagination.= "<li><a href='".add_query_arg(array('pg' => $counter))."'>{$counter}</a></li>";
            }

        } elseif($lastpg > 5 + ($adjacents * 2)){

            if($pg < 1 + ($adjacents * 2)) {

                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
                    if ($counter == $pg)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='".add_query_arg(array('pg' => $counter))."'>{$counter}</a></li>";
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => $lpm1))."'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => $lastpg))."'>{$lastpg}</a></li>";

            } elseif($lastpg - ($adjacents * 2) > $pg && $pg > ($adjacents * 2)) {

                $pagination.= "<li><a href='".add_query_arg(array('pg' => 1))."'>1</a></li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => 2))."'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $pg - $adjacents; $counter <= $pg + $adjacents; $counter++) {
                    if ($counter == $pg)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='".add_query_arg(array('pg' => $counter))."'>{$counter}</a></li>";
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => $lpm1))."'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => $lastpg))."'>{$lastpg}</a></li>";

            } else {

                $pagination.= "<li><a href='".add_query_arg(array('pg' => 1))."'>1</a></li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => 2)).">2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpg - (2 + ($adjacents * 2)); $counter <= $lastpg; $counter++) {
                    if ($counter == $pg)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='".add_query_arg(array('pg' => $counter))."'>{$counter}</a></li>";
                }
            }
        }

            if ($pg < $counter - 1) {
                $pagination.= "<li><a href='".add_query_arg(array('pg' => $next))."'>{$nextlabel}</a></li>";
                $pagination.= "<li><a href='".add_query_arg(array('pg' => $lastpg))."'>{$lastlabel}</a></li>";
            }

        $pagination.= "</ul>";
    }

    return $pagination;
}



function lp_delete_player() {
    $lp_opt =   get_option('lp_options');

    $id = $_POST['id'];

    echo $id;
    echo 'debug';
    global $wpdb;

    $wpdb->delete($lp_opt['players_table'], array('id'=> $id));

    wp_die();
}



function add_lp_scripts() {
    wp_enqueue_script('jquery','http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
    wp_enqueue_style('font-awesome','http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );
    wp_enqueue_style('lp_plugin',plugin_dir_url(__FILE__).'lp_style.css' );
    wp_enqueue_style('bootstrap-validator-css','http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.1/css/bootstrapValidator.min.css' );
    wp_enqueue_script('bootstrap-js','https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('bootstrap-validator-js','http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.1/js/bootstrapValidator.min.js', array('jquery'));
}




add_shortcode('lp_cosplay_form','lp_cosplay_form' );
add_action('wp_ajax_lp_delete_player', 'lp_delete_player');
add_action('wp_enqueue_scripts','add_lp_scripts');
add_shortcode('partners_widget','partners_widget');
add_filter('widget_text', 'do_shortcode');
add_shortcode('lp_form', 'lp_form');
add_action( 'admin_menu', 'lp_add_to_menu' );
register_activation_hook( __FILE__, 'lp_plugin_setup' );
register_deactivation_hook(__FILE__, 'lp_plugin_deactivation');




?>